<!doctype html>
<!-- html template originally from sw. slightly modified for rawk. -->
<html>
<head>
<title>${title}</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="${site_root}styles/style.css">
</head>
<body>
<div class="header">
<pre style="font-weight: bold;">
 _               __ _                  
| |_ _   _ _ __ / _(_)_ __   __ _ _ __ 
| __| | | | '__| |_| | '_ \ / _` | '__|
| |_| |_| | |  |  _| | | | | (_| | |   
 \__|\__, |_|  |_| |_|_| |_|\__, |_|   
     |___/                  |___/      
</pre>
<p>
    <a href="${root_link}">
      <span class="headerSubtitle">${subtitle}</span>
    </a>
</p>
</div>
<hr>
