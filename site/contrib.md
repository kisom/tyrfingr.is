# CONTRIB


## [SURFRAW](http://www.surfraw.org/)

**surfraw** is the shell user's revolutionary front rage against the web.

From the site: 

    "Surfraw provides a fast unix command line interface to a variety of popular 
    WWW search engines and other artifacts of power. It reclaims google, 
    altavista, babelfish, dejanews, freshmeat, research index, slashdot and many 
    others from the false-prophet, pox-infested heathen lands of html-forms, 
    placing these wonders where they belong, deep in unix heartland, as god 
    loving extensions to the shell." 

[rawk](/rawk/)'s name was inspired by [surfraw](http://www.surfraw.org/). 

Contributions include:
* writing quite a number of elvi, 
* general improvements, 
* cleaning up code.

I am a committer on the project.
