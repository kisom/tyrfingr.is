<pre style="text-align: center;">
 ____ ____ ____ ____ ____ ____ 
||e |||s |||s |||a |||y |||s ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|

</pre>

"**exceptions are overused** - i don't like the standard concept of exceptions.
Not yet written. (NULL)

"**lessons learned writing srvwd**" - in order to codify a few things i learned
whilst working on [srvwd](/projects/srvwd/), i put them into an essay format. 
Not yet written. (NULL)

"[minimalist web development](/essays/essay_minimal_webdev.html)" - minimalist
web development using git, rawk, srvwd, and vim. (2012.04.18)

"[the zen of c](/essays/essay_zen_of_c.html)" - on overthinking and the beauty 
of the c programming language. (2012.04.03)

"[suddenly enlightenment](/essays/essay_enlightenment.html)" - in which git
hooks are described, and hopefully elucidated slightly. (2011.12.03)

"[why i don't like the gpl](/essays/essay_no_gpl.html)" - wherein i try to
lucidly lay out my arguments against the gpl. (2011.09.14)

"[coders should do more than code](/essays/essay_coders_plus.html)" - coders
need to consider more than just the code they're writing. i consider build
systems, documentation, and support as important to the trade of making
software useful for other people. (2011.09.10)

"[why hack?](/essays/essay_why_hack.html)" - tl;dr: i hack because i can't not
hack. (2011.02.02)
