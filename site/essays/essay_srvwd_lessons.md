lessons learned from srvwd
==========================

* read the manpage
* memset
* when checking return values, a -1 can be checked with
    if (return_value)

  it's best to name these variables so they reflect did_fail semantics
* some implementations of dirname mutate the strings - strdup first
* debate of existence v. permissions check
* using callgrind / gprof / gcov with children
* expensiveness of functions
* challenge assumptions
* when in doubt, read the rfc
* makefiles
    * linking libs before objs
    * using sed for a config script
* reasons to hate GNU/Linux
    * strlcpy
    * extra build flags
