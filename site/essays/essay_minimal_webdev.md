## minimal web development with rawk and srvwd

I have next to no web development skills, but I still find it useful to
keep online project documentation around. Most of my sites feature a heavy
amount of text and are content-centric; static sites are the rule. I also like
reusing the tools that I already know - make(1), git(1) and so forth - to 
develop my sites. To that end, I wrote two tools that greatly expedite getting
content online.

## the tools

### rawk
[rawk](/projects/rawk/) is a static site generator that converts trees of 
markdown files into a site. it is written entirely in sh(1), and is POSIX 
compliant to work as portably as possible. It ships with a basic Makefile 
(compatible with both BSDmake and GNUmake) and a skeleton to quickly get sites 
started.

[rawk](/projects/rawk/) does have a [README](http://rawk.brokenlcd.net/README.html) 
and a [tutorial](http://rawk.brokenlcd.net/tutorial.html) that go over setting 
up a rawk project, including a quick start. I won't go over the specifics here.

### srvwd
For the longest time, I couldn't really test my rawk-generated sites out without
running a heavy webserver on my box. In order to facilitate developing sites
with rawk, [srvwd](/projects/srvwd/) was born. [srvwd](/projects/srvwd/) is a 
very bare-bones web server lacking many features. What it does offer is the 
ability to serve a static site from the command line, and by default sets the 
site root to the current working directory (or a path specified on the command 
line). It is not designed to be a production web server (there are plenty of 
choices for that - I use [nginx](http://www.nginx.org), and to a lesser extent
[apache](http://httpd.apache.org), for production web servers). It does fit the
role of being a fast test server, however.

## tmux
[tmux](http://tmux.sourceforge.net/) is a superior terminal multiplexer. Most
of my terminals run tmux sessions (for example: right now, I have six tmux
sessions with 33 windows).

## vim
[vim](http://www.vim.org/) is my go-to editor, and I've used them all.


## workflow
The first step is obviously determining that a site is needed. Once that's done,
I set up the DNS and remote web server with a target. Then, I create a new
project directory, set up source control (all my sites are currently using
[git](http://www.git-scm.com)), and copy over the [rawk](/projects/rawk/) 
skeleton files. 

[rawk](/projects/rawk/) requires minimal initial configuration: edit the 
Makefile with the appropriate remote server, mkdir site, and edit rawkrc with 
the site's title and subtitle. rawk ships with a decent stylesheet right out 
of the box (I hate tweaking CSS), and usually that's good enough. 

The actual site development then consists of adding in appropriate content:
pages are markdown, so a good first step is adding a relevant site/index.md.
I pull up a tmux session and vim; one pane in tmux is for building, and one is
for [srvwd](/projects/srvwd/). 

For example, 
    
    $ printf "# project\n\nThis is project that does foo and bar\n" > site/index.md

**srvwd** requires the initial build directory to be built, so a 'make' issued
in the project top level is in order. Pull up the pane dedicated to **srvwd**,
cd to site.build, and issue 'srvwd'. You can then pull up localhost:8080 in
the browser, and enter the development loop:

0. add / edit content
0. make
0. refresh the page
0. repeat as desired

## conclusion
Web development is now a lot less of a pain to me. What's great is that I now
use tools I wrote which do exactly what I need them to do. Coding is awesome.

## screenshot
There is a [screenshot](http://i.kyleisom.net/tyrfingr/minimal_webdev.png)
available for the interested.

2012.04.18
