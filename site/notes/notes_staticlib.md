# writing a static library in C

There is a barebones library that just prints hello, but shows how to write
the Makefile. It includes a test program to demonstrate the Makefile aspects.
This code is **extremely** simplistic and serves mainly to focus on writing
the Makefile. You can download the [tarball](/distfiles/misc/mylib.tar.gz)
and verify its [signature](/distfiles/misc/mylib.tar.gz.sig).

### The makefile:

    CC := gcc 
    CFLAGS += -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align
    CFLAGS += -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations
    CFLAGS += -Wnested-externs -Winline -Wno-long-long  -Wunused-variable
    CFLAGS += -Wstrict-prototypes -Werror -ansi -D_XOPEN_SOURCE=600
    TARGETS := libmylib.a
    OBJS := mylib.o
    
    all: $(TARGETS)
    
    libmylib.a: $(OBJS)
    	    ar rcs $@ $?
    
    clean:
    	    -rm $(OBJS) $(TARGETS)


### Explained:

> `CC := gcc`    
> `CFLAGS += -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align`    
> `CFLAGS += -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations`   
> `CFLAGS += -Wnested-externs -Winline -Wno-long-long  -Wunused-variable`   
> `CFLAGS += -Wstrict-prototypes -Werror -ansi -D_XOPEN_SOURCE=600`   
 
The first step is to define our C compiler and set up the build flags. I use 
very strict compiler flags, and try to ensure my code is 
[SuSv3](http://pubs.opengroup.org/onlinepubs/009695399/) compliant. 

> `TARGETS := libmylib.a`    
> `OBJS := mylib.o`    

For this simple example, the targets and object variables are trivial. 

> `all: $(TARGETS)`    
>    
> `clean:`    
> `        -rm $(OBJS) $(TARGETS)`    

These are two fairly standard targets. 

> `libmylib.a: $(OBJS)`    
> `        ar rcs $@ $?`

Now we get to the crux of the Makefile - the call to ar. At this point, as
the code has been compiled down into object files, all that remains is to
create the library archive. The options used here are:

* *r*: add or replace an archive member
* *c*: suppresses an informational message when an archive is created
* *s*: write the object-file index to an archive (ar s is the same as
running [ranlib](http://www.openbsd.org/cgi-bin/man.cgi?query=ranlib&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html) on a library archive)


(2012.05.21)
