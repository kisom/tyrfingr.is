## generating patchfiles with git and hg

UPDATE: originally this post was only about doing this in git. Since I use
mercurial almost as much as I use git, I decided to look into how to do it
with mercurial too.

I found myself needing to generate a patchfile today from a git repo. it turns
out to be a very easy task.

* first, commit to a clean working directory. i'll asume you are on the 
'master' (git) or 'default' (hg) branch, but s/master/$branch/ as appropriate.

* if you have only one commit between you and the commit you need to diff 
against: 

    git format-patch master^ --stdout > my.patch 

    hg export tip > my.patch    

* otherwise, substitute in the appropriate commit

* to apply the patch, it's 

    git apply --stat my.patch 

    hg patch my.patch

I did say it was a very easy task... You'll notice mercurial makes this easier
(or at least I think so) than git.

2011.09.28 
