# The Hello World Module

Init function: prepare the module for future calls to its function and alert
kernel to the fact that the module exists.

Exit function: alert the kernel that the module will no longer be available
and perform any final cleanup. If any resources are left, 


## Building Out-of-Tree Kernel Modules

kbuild: Linux kernel build system

    make -C ${kernel_source_path} M=$(pwd)
    make -C /lib/modules/$(uname -r)/build M=$(pwd)
    # add target "modules_install" to install the modules

options:
* `-C ${KDIR}` -- path to kernel source
* `-M $(pwd)`  -- path to module source

targets:
* modules (default)
* modules_install
* default
* help

### Creating Kbuild Files

all Kbuild files require the line

    obj-m := $(module_name).o

If there are multiple source files for the module:

    $(module_name)-y := src1.o src2.o

### Shared Makefile

filter out the test targets:

    ifneq ($(KERNELRELEASE), )
    obj-m := ...
    else
    ...
    endif

newer versions first look for Kbuild file, then Makefile; the Makefile
can be separate from the Kbuild file.

use `insmod` to insert a module:

* `sys_init_module` syscall defined in `kernel/module.c`
* allocates kmem with vmalloc
* copies module text into memory region
* resolves kernel references in modules via kernel symbol table
* calls module's init
* rmmod fails if module is still in use
* lsmod uses /proc/modules
* kernel API tends to be fluid

`linux/version.h` macros:
* LINUX_VERSION_CODE: kernel version in binary, three bytes
representing major, minor, release
* KERNEL_VERSION(major, minor, release) expands to appropriate Linux
version
  
platform specific code should go in separate files.

### vermagic.o
Kbuild links against vermagic.o containing precompiled kernel
information; when kmod is loaded, kernel checks module against this
and refuses to load if it doesn't match.


### Kernel Symbol Table
* contains addresses of global kernel items
* if module has useful functionality, it can export symbols -- modules
  may export symbols using those symbols ('module stacking')
* modprobe loads dependencies for these stacks

#### Exporting Symbols
* `EXPORT_SYMBOL(name)`
* `EXPORT_SYMBOL_GPL(name)` -- exports only to GPL modules
* export macro must be a global statement
* variable stored in ELF headers (details in `linux/module.h`)

### Preliminaries
* two includes in virtually all kmods:
0. `linux/module.h`
0. `linux/init.h`
* `linux/moduleparam.h` used to pass parameters to module at load time
* `MODULE_LICENSE`: one of "GPL", "GPL v2", "GPL and additional
  rights", "Dual BSD/GPL", "Dual MPL/GPL", "Proprietary".
* kernel is considered tainted if a non-GPL-license is loaded.
* `MODULE_AUTHOR`, `MODULE_DESCRIPTION`, `MODULE_VERSION`,
  `MODULE_ALIAS`, `MODULE_DEVICE_TABLE`
* a recent convention is to place MODULE_* at the end of the file

### Module Initialisation
* init function registers any facility (new functionality) that can be
  accessed by an application
* init function should be declared static

    static int __init init_function(void)
    {
        /* initialisation code goes here */
    }
    module_init(init_function)l

* `__init` (and `__initdata`) marks code as being for initialisation
  only; symbols are discarded after load
* most registration functions are prefixed with `register_`

### Cleanup

    static void __exit cleanup_function(void)
    {
        /* cleanup code */
    }
    module_exit(cleanup_function);

* `__exit` places code in a special ELF section
* if there isn't a cleanup function, the module cannot be unloaded
  (see the `noexit` module in this directory)
* Be paranoid about error handling
* should provide whatever capabilities possible, even in the case of
  degraded functionality (some functionality is better than no
  functionality)

### Initialisation Failures
* must undo any registrations
* failure to do so leaves dangling pointers and resource leaks than
  can only be recovered with a system reboot
* consider using `goto`
* immediately after a facility is registered, it is available for use
  by the kernel. other code may start using it immediately upon
  registration.
* don't register a facility until it is fully ready to be used
* if initialisation fails, but another part of the kernel is using an
  already-registered facility, a module loading race condition can
  occur.


## Module Parameters
parameter values can be passed via both `insmod` and `modprobe`; `modprobe`
also supports loading parameteres via `/etc/modprobe.conf`. for example,
given a modification to the `hello` module (named `hellop`) with parameters
`whom` and `howmany` (which will great `whom` `howmany` times):

    $ sudo insmod hellop howmany=10 whom="World"

making parameters available is done with the `module_param` macro, found in
`moduleparam.h`:

    module_param(name, type, sysfs permissions);

it is a global call. supported parameter types:

* bool
* invbool (inverts value)
* charp (char pointer)
* int
* long
* short
* uint
* ulong
* ushort

array parameters:

    module_param_array(nanme, type, nump, perm);

nump is an integer variable that contains the number of elements at load
time. the module will not load more elements than fit.

## User Space Drivers

benefits to user space modules:

0. full C library linkage
0. developer can use a normal debugger
0. hung userspace drivers can be killed, entire system unlikely to be affected
0. swappable memory, which is useful for drivers that use a lot of ram
0. a properly designed userspace driver can still provide concurrent device
access
0. more friendly to !GPL

the typical process is to write a server process, and let clients communicate
with that. there are drawbacks, though:

0. interrupts are unavailable (but see vm86 syscall on IA32)
0. DMA is unavailable except to privileged users
0. I/O port access is only available after a call to `ioperm` or `iopl`, but
not all platforms support this, and accessing `/dev/port` can be too slow;
permissions rear their head here as well
0. repsonse time is slower due to context switch
0. swapping to disk creates unacceptable delays; `mlock` sometimes helps,
but a lot of memory pages need to be locked for library code (and again,
mlock can only be used by privileged users, specifically those with
CAP_IPC_LOCK)
0. most devices can't be handled in user space

because of these drawbacks, userspace drivers are typically heavily restricted
in their functionality; there are some interesting uses that rely on other
kernel drivers (like a generic USB driver). they should be considered when
working with new hardware to limit their impact on a system.

