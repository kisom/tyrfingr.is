# char drivers

this entire chapter is dedicated to the `scull` device driver we'll develop;
`scull` stands for "simple character utility for loading localities". `scull`
acts on a memory area as if it were a device. for this chapter, device is
used interchangeably with "memory area used by `scull`". because it uses
a memory area, it isn't hardware dependent; coincidentally, it doesn't use
anything useful.

## Design of `scull`

the first step is to define the capabilities the driver will offer to
users. for `scull`, some options to be considered are sequential
v. random-access, the number of device files.

we will implement the following devices (each kind implemented is
termed a *type*):

* `scull[0-3]` -- globally persistent memory area: data is shared
  between all open file descriptors and data isn't lost on descriptor
  close.
* `scullpipe[0-3]` -- FIFO devices that act like pipes: one process
  will read what another process outputs. multiple reading proceesses
  will lead to data contention.
* `scullsingle` -- globally persistent but can only be opened by one
  process at a time.
* `scullpriv` -- globally persistent but private to each virtual
  console
* `sculluid` -- globally persistent but can only be opened by one user
  at a time; returns EBUSY if the device is in use.
* `scullwuid` -- similar to `sculluid` but implements a blocking wait

note that only the first device is implemented in this chapter.

## Major and Minor Numbers

traditionally, the major number identifies the driver associated with
the device, and the minor number refers to a specific device. for
example, on this machine, `/dev/sda` has the device number `8, 0`
while `/dev/sdb` has the device number `8, 16`. the kernel doesn't
really concern itself with the minor number.

## Internal Representation of Device Numbers

the `dev_t` type (from `linux/types.h`) holds device numbers. don't
rely on the specifics; use the macros in `linux/kdev_t.h`:

* major can be obtained from `MAJOR(dev_t dev);`
* minor can be obtained from `MINOR(dev_t dev);`
* major, minor -> dev_t from `MKDEV(int major, int minor);`

## Allocating and Freeing Device Numbers

char drivers need to obtain device numbers with the
`register_chrdev_region` from `linux/fs.h`:

    int register_chrdev_region(dev_t first, unsigned int count,
                               char *name);

`first` is the first device number to allocate; only the major number
matters. `count` is the number of contiguous devices being
requested. if `count` is too large, request might spill over to next
major number. name is the device that should be associated with the
range; it will appear in `/proc/devices` and sysfs. 

there is a move to dynamically allocated device numbers; this is done
with a different function:

    int alloc_chrdev_region(dev_t *dev, unsigned int firstminor,
                            unsigned int count, char *name);

dev is an output-only parameter holding the first number in the
allocated region. firstmoniro will typically be zero.

these device numbers are both unregistered with the same function:

    /* should be in the module's cleanup function */
    void unregister_chrdev_region(dev_t first, unsigned int count);

once allocated, these numbers still need to be connected to the
appropriate functions.

## Dynamic Allocation of Major Numbers

`Documentation/devices.txt` describes the statically assigned major
numbers for common devices (ex. console, disk drivers,
etc...). although new numbers are not being allocated, it is still
strongly recommended to dynamically allocate device numbers.

the disadvantage of dynamic allocation is that device nodes can't be
created in advanced (i.e. with `mknod`). once the number is assigned,
it can be found in `/proc/devices`.

**Note**: the book talks about replacing insmod with a script that
calls insmod, then reads `/proc/devices`, and then creates the special
file(s):

         #!/bin/sh
         # scull_load is distributed with scull in the book
         module="scull"
         device="scull"
         mode="664"
    
         # invoke insmod with all arguments we got
         # and use a pathname, as newer modutils don't look in . by default
         /sbin/insmod ./$module.ko $* || exit 1
    
         # remove stale nodes
         rm -f /dev/${device}[0-3]
    
         major=$(awk "\$2==\"$module\" {print \$1}" /proc/devices)
    
         mknod /dev/${device}0 c $major 0
         mknod /dev/${device}1 c $major 1
         mknod /dev/${device}2 c $major 2
         mknod /dev/${device}3 c $major 3
    
         # give appropriate group/permissions, and change the group.
         # Not all distributions have staff, some have "wheel" instead.
         group="staff"
         grep -q '^staff:' /etc/group || group="wheel"
    
         chgrp $group /dev/${device}[0-3]
         chmod $mode  /dev/${device}[0-3]
         
         ## to adapt this for other drivers, redefine the variables and
         ## adjust mknod lines.

chapter 6 details access control on a device.

in scull, here's an example to get a major number:

        if (scull_major) {
             dev = MKDEV(scull_major, scull_minor);
             result = register_chrdev_region(dev, scull_nr_devs, "scull");
         } else {
             result = alloc_chrdev_region(&dev, scull_minor, scull_nr_devs,
                                          "scull");
             scull_major = MAJOR(dev);
         }
         if (result < 0) {
             printk(KERN_WARNING "scull: can't get major %d\n", scull_major);
             return result;
         }

## Some Important Data Structures

there are three important kernel data structures required by most of
the fundamental device driver operations. these three structures are

0. `file_operations`
0. `file`
0. `inode`

### File Operations

`file_operations` is the means in which a character driver connects
the registered major number to the driver's functions. the structure
is defined in `linux/fs.h` and is a collection of function
pointers. each open file is associated with its own set of
functions. the operations generally implement the syscalls
(ex. *open*, *read*, etc...)

traditionally, this `file_operations` is termed
`struct file_operations *fops`. if a field in the structure is NULL,
it indicates an unsupported operation.

the string __user noting that a pointer is a user-space address that
can't be directly dereferenced; it is mostly used to detect abuse of
user-space addresses.

file_operations methods:

* `struct module *owner` -- pointer to the module owning the structure
  (prevents module from being unloaded while in use). typically
  initialised to `THIS_MODULE`, defined in `linux/module.h`
* `loff_t (*llseek) (struct file *, loff_t, int);` -- change the
  current r/w position in a file; new position is a positive return
  value. errors are a negative return value. if the the function
  pointer is NULL, seek calls modify the position counter in the file
  structure in unpredictable ways.
* `ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);`
  -- implements `read`; if NULL, returns `-EINVAL`.
* `ssize_t (*aio_read) (struct kiocb *, char __user *, size_t,
  loff_t);` -- async read; if NULL, reads are synchronously processed
  with `read`.
* `(*readdir) (struct file *, void *, filldir_t);` -- NULL for device
  files, is used for filesystems.
* `unsigned int (*poll) (struct file *, struct poll_table_struct *);`
  -- poll method is the backend to three system calls: `poll`,
  `epoll`, and `select`; should return a bit mask that indicates
  whether nonblocking I/O is possible and how the process should
  sleep. if NULL, device is assumed to be both readable and writable
  without blocking.
* `int (*ioctl) (struct inode *, struct file *, unsigned int, unsigned
  long) -- issue device-specific commands; returns ENOTTY if the
  `poll` method is NULL.
* `int (*mmap) (struct file *, struct vm_area_struct *);` -- if NULL,
  returns `-ENODEV`.
* `int (*open) (struct inode *, struct file *);` -- if entry is NULL,
  opening device always succeeds but the driver isn't notified.
* `int (*flush) (struct file *);` -- invoked when process closes copy
  of a fd for a dev; should execute and wait for any outstanding
  operations on the dev. this is not the same as the `fsync`
  operation. if NULL, user application requests are ignored by the
  kernel.
* `int (*release) (struct inode *, struct file *);` -- invoked when
  file structure is being released, and may be NULL.
* `int (*fsync) (struct file *, struct dentry *, int)` -- backend of
  `fsync` call, which flushes any pending data. if NULL, returns
  `-EINVAL`.
* `int (*aio_fsync) (struct kiocb *, int);` -- async `fsync`.
* `int (*fasync) (int, struct file *, int);` -- notify device of a
  change in ASYNC flag. can be NULL.
* `int (*lock) (struct file *, int, struct file_lock *);` -- file
  locking; almost never implemented by drivers.
* `ssize_t (*readv) (struct file *, const struct iovec *, unsigned
  long, loff_t *);`
* `ssize_t (*writev) (struct file *, const struct iovec *, unsigned
  long, loff_t *);` -- scatter/gather I/O; if NULL, `read` and `write`
  are called instead.
* `ssize_t (*sendfile) (struct file *, loff_t *, size_t, read_actor_t,
  void *);` -- read size of `sendfile` syscall which moves the data
  from one file descriptor to another with a minimum of
  copying. generally not implemented by device drivers.
* `ssize_t (*sendpage) (struct file *, struct page *, int, size_t,
  loff_t);` -- other half of `sendfile`; typically not implemented by
  device drivers.
* `unsigned long (*get_unmapped_area) (struct file *, unsigned long,
  unsigned long, unsigned long, unsigned long);` -- find a suitable
  location in process address space to map in a memory segment. it is
  meant to enforce alignment requirements a device might
  have. typically safe to set to NULL.
* `int (*check_flags) (int);` -- allows module to check flags passed
  to an `fcntl(F_SETFL)` call.
* `int (*dir_notify) (struct file *, unsigned long);` -- request
  directory change notification; it is for filesystems, not generally
  implemented by drivers.

`scull` only implements the most important of these, so its
`file_operations` struct is initialised with:

    struct file_operations scull_fops = {
        .owner =      THIS_MODULE,
        .llseek =     scull_llseek,
        .read =       scull_read,
        .write =      scull_write,
        .ioctl =      scull_ioctl,
        .open =       scull_open,
        .release =    scull_release,
    };

tagged syntax is preferred for portability; there are some
optimisations can be had by placing pointers to frequently accessed
members in the same hardware cache line.

## The `file` Structure

`struct file` defined in `linux/fs.h` is the second most important
data structure used in device drivers. this is not the same as a FILE
pointer in user-space (which is defined in the C library) -- the
`struct file` never appears in user-space. it represents an *open
file descriptor*; ever open file on the system has an associated
kernel `struct file`. it is released on the last `close`. the pointer
is generally named `file` or `filp`.

the most important fields are:

* `mode_t f_mode`; -- identifies the file as either readable or
writable using the bits `FMODE_READ` and `FMODE_WRITE`. should be
checked in `open` and `ioctl`, but permissions aren't needed for
`read` and `write`.
* `loff_t f_pos;` -- the current R/W position and is 64 bits on all
platforms. it can read this, but shouldn't normally change
it. `read` and `write` should update the position. the exception is
`llseek`.
* `unsigned int t_flags;` -- file flags; perms should be checked with
`f_mode`.
* `struct file_operations *f_op;` -- never saved by kernel, can be
changed on the fly.
* `void *private_data;` -- open sets to NULL before calling open. this
field can be ignored, but it must be freed in the `release`
method. can be useful for preserving state in the module.
* `struct dentry *f_dentry` -- directory entry associated with files;
typically, only `filp->f_dentry->d_inode` is used by the device
driver.

there are more fields, but not useful to device drivers.

### The inode structure

Used by the kernel internally to represent files. generally, only two
fields are of note for device drivers:

* `dev_t i_rdev` -- actual device number
* `struct cdev *i_cdev` -- internal structure representing char
  devices.

two macros can be used to obtain the major and minor numbers from an
inode:

* `unsigned int iminor(struct inode *inode);`
* `unsigned int imajor(struct inode *inode);`

should be used instead of manipulating `i_rdev`.

## Character Device Registration

`struct cdev` is a kernel representation of char devices internally;
the definition can be found in `linux/cdev.h`. (there is an older
method, but new code shouldn't use it.) two ways of allocating and
initialising one:

    /* standalone runtime cdev structure */
    struct cdev *my_cdev = cdev_alloc();
    my_cdev->ops = &my_fops;

embedding the `cdev` structure in a device-specific structure
(*scull* does this):

    void cdev_init(struct cdev *cdev, struct file_operations *fops);

also need to set cdev->owner --

    cdev->owner = THIS_MODULE;

final step: tell the kernel about it --

    int cdev_add(struct cdev *dev, dev_t num, unsigned int count);

`num` is first device number the device responds to, count is the
number of device numbers to associate. the call can fail; a negative
return code means the device was not added. as soon as the call
returns, the device is "live" and operations can be called with the
kernel. the device is removed with

    void cdev_del(struct cdev *dev);

### Device Registration in `scull`

internally, `scull` defines a `struct scull_dev`:

    struct scull_dev {
            struct scull_qset *data;       /* pointer to first quantum set */
            int quantum;                   /* the current quantum size */
            int qset;                      /* the current array size */
            unsigned long size;            /* amount of data stored */
            unsigned long int access_key;  /* used by sculluid and scullwuid */
            struct semaphore sem;          /* mutex semaphore */
            struct cdev cdev;              /* char device structure */
    }
    
    static void scull_setup_cdev(struct scull_dev *dev, int index)
    {
            int err, devno = MKDEV(scull_major, scull_minor + index);
            
            cdev_init(&dev->cdev, &scull_fops);
            dev->cdev.owner = THIS_MODULE;
            dev->cdev.ops = &scull_fops;
            err = cdev_add(&dev->cdev, devno, 1);
            
            /* fail gracefully as required */
            if (err)
                    printk(KERN_NOTICE "Error %d adding scull%d", err, index);
    }

### The Older Way

registering a char dev:

    int register_chrdev(unsigned int major, const char *name,
                        struct file_operations *fops);

drivers must be prepared to handle open calls on a 256 minor numbers,
and can't use major/minor > 255. it requires

    int unregister_chrdev(unsigned int major, const char *name);

where `major` and `minor` must be the same as those passed to
`register_chrdev`.

## open and release

### The `open` Method

`open` is provided for a driver to do any intialisation in preparation
for later operations, and in most drivers, should do:

* check for device specific errors
* initialise the device if opened for the first time
* update the `f_op` as required
* allocate and fill any structures that should be in
`filp->private_data`.
  
first step is to indentify the device being opened; recalling function
signature (`int (*open) (struct inode *inode, struct file *filp);`),
the `inode`'s `i_cdev` field. to get at the `scull_dev` structure, use
the `container_of` macro found in `linux/kernel.h`:

    container_of(pointer, container_type, container_field);

for example:

    struct scull_dev *dev;      /* device information */
    dev = container_of(inode->i_cdev, struct scull_dev, cdev);
    filp->private_data = dev;   /* for other methods */

the other way is to look at the minor number stored in the `inode`
structure. devices registered with `register_chrdev` are required to
use this technique. use `iminor` to obtain minor number from the
`inode` structure. you must validate that it corresponds to a device
the driver can handle. a slightly simplified version of `scull_open`
would be

    int scull_open(struct inode *inode, struct file *filp)
    {
            struct scull_dev *dev;          /* device information */
            
            dev = container_of(inode->i_cdev, struct scull_dev, cdev);
            filp->private_data = dev;       /* for other methods */
            
            /* if write-only, truncate device length to 0 */
            if ((filp->f_flags * O_ACCMODE) == O_WRONLY)
                    scull_trim(dev);        /* ignore errors */
            
            return 0;
    }


there is no particular device handling as the device is global and
persistent, and no action such as init on first open, as there is no
open count for `scull`s.

the truncate is performed is truncating to a length of 0 when opened
on write. overwriting a `scull` device with a shorter file results in
a shorter device data area, similar to the model with a regular
file. a real init will be seen with other `scull` personalities.

### The `release` method

opposite of open; sometimes named `device_close` instead of
`device_release`; its tasks should be:

* deallocate anything that `open` allocated in `filp->private_data`
* shutdown device on last close

basic `scull` device has no hardware to shutdown, so:

    int scull_release(struct inode *inode, struct file *filp)
    {
            return 0;
    }

the other versions of the device use different functions as
`scull_open` substitutes a different `filp->f_op` for each device,
which will be covered in each new flavour.

not every `close` call triggers `release` method. only calls that
actually release the device data structure invoke it. kernel keeps
track of how many times a `file` structures exist; only `open` creates
a new `file` structure. the `release` call only executes `release`
when the `file` structure drops to 0. `flush` is called each time
`close` is called. this only needs to be defined if something needs to
be done apart from a `release`. note the kernel automatically closes
any file at process exit.

## `scull`'s Memory Usage

right now, we deal with only a basic memory allocation module, and not
hardware management. two core functions are used to manage kernel
memory, defined in `linux/slab.h`:

    void *kmalloc(size_t size, int flags);
    void  kfree(void *ptr);

kmalloc is similar to malloc; flags describe how memory is
allocated. later, other flags will be described; for now stick to
`GFP_KERNEL`. allocated memory (and only allocated memory) must be
passed to `kfree`; a NULL pointer is acceptable. `kmalloc` isn't the
most efficient way to allocate kernel memory. 

in `scull`, each device is a linked list of pointers to a `scull_qset`
structure; each is limited to 4 MB; the source uses an array of 1000
pointers to areas of 4kb. each memory area is a *quantum*, and array
of these is a *quantum set*. writing a single byte consumes 8kb or
12kb: 4kb for the quantum and 4kb or 8kb for *qset* depending on
pointer size. choosing appropriate values is a question of policy,
rather than mechanism. 

the *qset*s are defined as:

    struct scull_qset {
            void **data;
            struct scull_qset *next;
    }

`scull_trim` is used to free the data area:

    int scull_trim(struct scull_dev *dev)
    {
            struct scull_qset *next, *dptr;
            
            int qset = dev->qset;           /* "dev" is not NULL */
            int i;
            
            for (dptr = dev->data; dptr; dptr = next) {
                    if (dptr->data) {
                            if (i = 0; i < qset; i++)
                                    kfree(dptr->data[i]);
                            kfree(dptr->data);
                            dptr->data = NULL;
                    }
                    next = dptr->next;
                    kfree(dptr);
            }
            dev->size = 0;
            dev->quantum = scull_quantum;
            dev->qset = scull_qset;
            dev->data = NULL;
            return 0;
    }

## `read` and `write`

both perform the task of copying data between driver and app code.

    ssize_t read(struct file *filp, char __user *buf, size_t count,
                 loff_t *offp);
    ssize_t write(struct file *filp, const char __user *buf, size_t count
                  loff_t *offp);

`buf`, being marked `__user`, is a user-space pointer; because
* depending on arch, pointer may not be valid in kernel mode
* user space memory is paged, and that memory might not be
  resident. the kernel is not allowed to generate a page fault.
* pointer has not been sanitised and could compromise security

access to user space buffers must be done through special
kernel-supplied functions; these are defined in `asm/uaccess.h`. some
will be covered now, others in a later chapter. 

copyinging and writing to user-space buffers:

    unsigned long copy_to_user(void __user *to,
                               const void *from,
                               unsigned long count);
    unsigned long copy_from_user(void *to,
                                 const void *from,
                                 unsigned long count);

these functions also check whether the user-space pointer is valid. if
it is invalid, no copy is performed; an invalid pointer causes only
part of the data to be copied. return value is amount of data
remaining to be recovered; `scull` looks for this error and returns
-EFAULT if not all of the memory could be copied. `__copy_to_user` and
`__copy_from_user` can be used if the pointer doesn't need to be
checked.

the `read` and `write` methods should update file position at `*offp`
to represent file position on success. `pread` and `pwrite` have
different semantics, calls in a pointer to a user-supplied position
and discard changes made by the driver. they return a negative value
on error. an error with some data copied isn't reported until the next
time the function is called. implementing this requires the driver to
store the error, so it may be returned in the future.


