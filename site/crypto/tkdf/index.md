## TKDF: A Timestamp-based KDF For Storing Secrets

### Quick Links

* [Version 1.0.1 of the specification](/crypto/tkdf/spec_v1.0.1.txt)
* [The reference Go implementation](https://hg.tyrfingr.is/kyle/tkdf)
  (and [Github mirror](https://github.com/cryptobox/tkdf/))
* [The Python implementation](https://github.com/cryptobox/pytkdf/)
  (available on [PyPI](https://pypi.python.org/pypi/pytkdf) as well).

### Background

TKDF specifies a key-derivation system based on Scrypt and the
Cryptobox cryptographic system. This KDF generates new keys based
on high-resolution timestamps. It was designed in particular for
securing cryptographic secrets as part of a software key storage
system. The storage of cryptographic secrets is a continuing challenge
in building secure systems. One of the major difficulties is in the
process of re-keying; this KDF presents a solution to this problem.

### Security Model

This KDF is designed to be used on a key storage server, specifically
to encrypt encryption keys for storage in a database. It is assumed
that an attacker will not have access to the memory used by the
key; however, if the database is dumped, an attack should not be
able to decrypt the keys. The master key should be generated using
a strong key derivation algorithm.

As the data to be encrypted should be randomly-generated encryption
keys, there is a very low probability that the same information
will be encrypted with the same encryption key. It is therefore
less of a concern if more than one element shares a CEK.

It is also important to note that this algorithm is designed to
store data-at-rest; if TKDF is being used to store cryptographic
secrets, a retrieved secret must be protected. Keys MUST NOT be
transmitted over insecure channels.

A single key can encrypt 2<sup>64</sup> blocks of data; this
translates to 2<sup>39</sup> 8-byte AES-256 + HMAC-SHA-384 keys on
a single key. Assuming randomly generated encryption keys are being
stored, there is an astronomically low probability that the same
messages will be encrypted twice. This means that, even with
lower-resolution timers with a resolution of perhaps one millisecond
(or even a single second), sharing a key for a number of messages
has a much lower probability of cryptographic compromise.

There are two other benefits to basing derived keys on timestamps:
state can be stored along with the secret, and concurrent cryptographic
operations can take place, as no state must be updated with the
key.

### Comments

The author can be reached at kyle at the tyrfingr domain.
