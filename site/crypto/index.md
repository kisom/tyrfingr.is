## CRYPTO

My passion is designing and engineering secure systems. To that
end, I've written a fair amount of cryptographic code. This page
an overview of tools to facilitate building secure systems, and
some of those systems.

### CRYPTOBOX

[CryptoBox](https://cryptobox.tyrfingr.is/) is the system that I
have put the most effort into. It aims to provide a toolkit for
developing secure systems without worrying about cryptographic
parameter selection (such as key lengths, cipher suites, etc...)
It is written in Go.

* The original
[talk](http://talks.gokyle.org/denver.gophers/2013/cryptobox.slide) and
[article](http://talks.gokyle.org/misc/cryptobox.article) describe
the motivations and some of the design choices that were made.

* The CryptoBox
[specifications](https://cryptobox.tyrfingr.is/files/cryptobox_spec.pdf)
and
[guidebook](https://cryptobox.tyrfingr.is/files/cryptobox_guidebook.pdf)
describe in detail the CryptoBox technical specifications; the
guidebook presents solutions to common problems. The guidebook also
comes with [example code](https://github.com/cryptobox/guidebook/).
The source code for both documents is [also
available](https://github.com/cryptobox/cryptobox-docs/).

* [gocryptobox](https://hg.tyrfingr.is/kyle/gocryptobox/)
([mirrored on Github](https://github.com/cryptobox/gocryptobox/))
is the reference CryptoBox implementation, written in Go.

* [xtsbox](https://hg.tyrfingr.is/kyle/xtsbox/) ([mirrored on
Github](https://github.com/cryptobox/xtsbox/)) is an implementation
of the CryptoBox "strongbox" module using AES256-XTS with HMAC-SHA-384
message tags.

* [gcmbox](https://hg.tyrfingr.is/kyle/gcmbox) ([mirrored on
Github](https://github.com/cryptobox/gcmbox)) is an implementation of
the CryptoBox "strongbox" module using AES256-GCM with HMAC-SHA-384
message tags.

* [streambox](https://hg.tyrfingr.is/kyle/streambox) ([mirrored on
Github](https://github.com/cryptobox/streambox)) is a Go package
implementing secure sessions over an io.ReadWriteCloser.

* [cbecdsa](https://hg.tyrfingr.is/kyle/cbecdsa) ([mirrored on
Github](https://github.com/cryptobox/cbecdsa/)) is a Go package
that provides a conversion interface between CryptoBox and Go's
ECDSA key format.

#### PORTS

While CryptoBox is currently Go-only, ports are planned to other languages.

* [pycryptobox](https://hg.tyrfingr.is/kyle/pycryptobox/) ([mirrored
on Github](https://github.com/cryptobox/pycryptobox/)) is an
in-progress port to Python.

* [libcryptobox](https://hg.tyrfingr.is/kyle/libcryptobox/) ([mirrored
on Github](https://github.com/cryptobox/libcryptobox/)) is an
in-progress port to C.

#### TKDF

[TKDF](/crypto/tkdf/) is a key-derivation system designed for storing
secrets. It uses high-resolution timestamp as a means of generating
content encryption keys from a key encryption key.

* The Go [tkdf](https://hg.tyrfingr.is/kyle/tkdf/) implementation
([mirrored on Github](https://github.com/cryptobox/tkdf/)) is the
reference implementation and includes the [spec](/crypto/tkdf/spec_v1.0.0.txt).

* There is also a [Python implementation](https://github.com/cryptobox/pytkdf/), which is [available
via PyPI](https://pypi.python.org/pypi/pytkdf/) (and, by extension, via
`pip`) as well.

#### ARX

Arx is a key server / storage system I've been working on. I finished a
first implementation in late December 2013. An [overview paper](/files/arx.pdf)
is available, and the source code is also
[available](https://hg.tyrfingr.is/kyle/arx) (and [mirrored on
Github](https://github.com/cryptobox/arx)).

#### NEXT-GEN

The Silent Circle blog post about non-NIST cipher suites had me reconsider
the cipher choices in CryptoBox. I've been working on the next generation
of CryptoBox; the current revision is using Twofish-256 in CTR mode with
Keccak-384 message tags (using the MAC construction discussed by the
Keccak authors); while I'd prefer Threefish and Keccak (or Threefish
and Skein), these aren't implemented in Go yet.

The current source code, with TKDF and Fortuna ported over, is
[available](https://hg.tyrfingr.is/kyle/cryptobox-ng) ([Github
mirror](https://github.com/cryptobox/cryptobox-ng/)).

### SYSTEMS

#### SSH-based

Part of my work has been to develope cryptographic systems based
on OpenSSH; particularly, with an aim to provide re-use for SSH
keys.

* [sshcrypt](https://hg.tyrfingr.is/kyle/sshcrypt/) is a fully
functional proof-of-concept stopgap that uses ECDSA SSH keys as a
replacement for PGP. I wrote a [blog post](http://kyleisom.net/blog/2013/10/28/sshcrypt/)
on `sshcrypt`. The end goal was to leverage an existing system,

* [sshbox](https://hg.tyrfingr.is/kyle/sshbox) was the precursor to
`sshcrypt`; this version supported both RSA and ECDSA.

### TOOLS

#### SSH-based

* [sshkey](https://hg.tyrfingr.is/kyle/sshkey/) is a Go package that
provides an interface to SSH keys. The proof-of-concept
[sshkeygen](https://hg.tyrfingr.is/kyle/sshkeygen) is a pure-Go
implementation of the basic features of `ssh-keygen(1)` using this
package.

#### Elliptic Curve

* [ecies](https://hg.tyrfingr.is/kyle/ecies/) is a Go implementation
of the elliptic curve integrated encryption scheme supporting the
NIST P256, P384, and P521 curves and various symmetric encryption
schemes.

* [ecdh](https://hg.tyrfingr.is/kyle/ecdh/) is a Go implementation
of the elliptic curve Diffie-Hellman key agreement method.

#### Miscellaneous

* [hkdf](https://hg.tyrfingr.is/kyle/hkdf) ([Github
mirror](https://github.com/gokyle/hkdf/)) is a Go implementation of the
RFC 5869 HMAC-based Extract-and-Expand Key Derivation Function.

* [catena](https://hg.tyrfingr.is/kyle/catena/) is a Go implementation
of the [Catena memory-consuming password scrambler](http://eprint.iacr.org/2013/525.pdf).

* [gofortuna](https://hg.tyrfingr.is/kyle/gofortuna/) is an
implementation of the Fortuna PRNG in Go. It includes an alternative
to the version specified in *Cryptography Engineering* called
`Tunafish`: it uses Twofish-256 and Keccak-256 instead of the AES-256
and SHA-256 used in the book.

* [hotp](https://hg.tyrfingr.is/kyle/hotp/) ([Github
mirror](https://github.com/gokyle/hotp)) is a Go implementation of RFC
4226 OATH-HOTP authentication.

* [dhkam](https://hg.tyrfingr.is/kyle/dhkam/) is an implementation
of the Diffie-Hellman key agreement method in Go. It uses the RFC
3526 Group 14, and employs blinded key generation.

* [libdhkam](https://hg.tyrfingr.is/kyle/libdhkam) is an implementation
of the Diffie-Hellman key agreement method in C, distributed as a
statically-compiled binary. It uses the RFC 3526 Group 14 and blinded
key generation.

### PUBLISHED

I've written some articles, and working on a book:

* *[Practical Cryptography with Go](https://leanpub.com/gocrypto/)*
is an introduction to cryptography for developers. It assumes that
the reader isn't looking for cryptographic theories, but practical
ways to use cryptography in their projects. It covers the basic
standard building blocks, and includes sample code to help readers
get started.

* [OpenSSH keys: a walkthrough](http://kyleisom.net/blog/2013/07/29/ssh-keys/)
(published 2013/07/29) covers how OpenSSH RSA and ECDSA keys are
stored on disk, and how to read them in code.

* [Securing Communications Between Peers](http://kyleisom.net/blog/2013/07/31/secure-communications/) 
(published 2013/07/31) presents a high-level look at techniques for
securely communicating between peers.

* [Post-Defcon Notes](http://kyleisom.net/blog/2013/08/04/post-defcon-thoughts/)
(published 2013/08/04) is a writeup on some thoughts I had after
attending the Blackhat and Defcon conferences.

* [Elliptic Curve Patents](http://kyleisom.net/blog/2013/08/05/elliptic-curve-patents/)
(published 2013/08/05) covers the state of elliptic curve patents.

* [sshcrypt: a proof-of-concept stopgap](http://kyleisom.net/blog/2013/10/28/sshcrypt/)
(published 2013/10/28) presents a new proof-of-concept set of tools
that attempt to provide a stop-gap balancing a distrust in the
finite-field cryptographic systems employed in PGP with elliptic-curve
based systems. It attempts to leverage an already-common elliptic
curve cryptosystem, OpenSSH, as the key system.

* [A Working Introduction to Crypto with
PyCrypto](http://kyleisom.net/blog/2011/06/17/intro-to-crypto) (also
published on [Leanpub](https://leanpub.com/pycrypto/)) is the ancestor to
[Practical Cryptography with Go](https://leanpub.com/gocrypto). This
was immediately by an effort to write the [book in
C](https://github.com/kisom/c_crypto_intro).
