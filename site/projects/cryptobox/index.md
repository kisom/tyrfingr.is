## cryptobox(3)

CryptoBox is a project underway to develop a package that provides core
functions for safely employing cryptography, while using FIPS ciphers
as the underlying cryptographic primitives. It is heavily inspired by
[NaCL](http://nacl.cace-project.eu/) The aim is provide developers with
a well-documented, properly-written, FIPS-compliant set of tools for
proper encryption. Most developers will want to use NaCL, but this
is provided for scenarios in which the use of Suite B ciphers might
be required.

### Packages

All of these packages are tested to be interoperable between each other.

* [Go package](https://github.com/gokyle/cryptobox)
* [Python package](https://pypi.python.org/pypi/cryptobox) 
  ([source](https://github.com/kisom/pycryptobox/))
* C package, using libgcrypt, is under development.

### Licensing

CryptoBox packages must be ISC-licensed as part of the standard.

### Module Overview

There are four modules provided in a CryptoBox distribution:

* box: message encryption and authentication with public-key
  cryptography. Provides 20-year (128-bit) security. This is suitable
  for securing traffic between two parties.
* secretbox: small message encryption and authentication using secret-key
  cryptography. Provides 20-year (128-bit) security. This is suitable
  for local security.
* stoutbox: message encryption and authentication with public-key
  cryptography. Provides 50-year (256-bit) securitym suitable for securing
  traffic between two parties.
* strongbox: small message encryption and authentication using secret-key
  cryptography. Provides 50-year (256-bit) security. This is suitable
  for local security.

These modules provide "boxes", or secured messages. The boxes are not
compatible between modules; that is, a box sealed with `secretbox`
cannot be opened with `cryptobox`.

### API Overview

Each module provides the following functions:

* `GenerateKey` for generating proper keys,
* `KeyIsSuitable` for determining whether a key is suitable for the module,
* `Seal` to seal a message, and
* `Open` to open a sealed message.

The `cryptobox` and `strongkit` modules also provide

* `SignAndSeal` to digitally sign the message before sealing it, and
* `OpenAndVerify` to open a signed box and check the digital signature.

An effort has been made to keep the function calls as similar as possible
between languages, while still retaining idiomatic for that language. Each
module is also documented.

### Port Status

* Go: complete
* Python: secretbox and strongbox are complete.
* C: initial development has begun.

### Cipher Suites

* secretbox: messages are encrypted with AES-128 in CTR mode, with
  HMAC-SHA-256 message tags.
* strongbox: messages are encrypted with AES-256 in CTR mode, with
  HMAC-SHA-256 message tags.
* box: messages are encrypted using ECDH over the P256 elliptic
  curve to generate shared keys; secretbox is used as the underlying
  cryptographic system.
* stoutbox: messages are encrypted using ECDH over the P521 elliptic
  curve to generate shared keys; strongbox is used as the underlying
  cryptographic system.
