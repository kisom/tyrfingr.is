# SRVWD(1)
### performance metrics


## TEST ENVIRONMENT
The tests were run on a [progmr](http://www.prgmr.com/) VPS running
[Debian](http://www.debian.org/) 5.0. I ran [srvwd](/srvwd/) on my 
home directory and served the file 'index.html'.

I used the [ab](http://httpd.apache.org/docs/2.2/programs/ab.html) utility 
(aka the apache benchmark tool) with the following
command line:

    $ ab -n 10000 -c 10 http://127.0.0.1:8080/index.html


## SRVWD RESULTS

    Concurrency Level:      10
    Time taken for tests:   11.767 seconds
    Complete requests:      10000
    Failed requests:        0
    Write errors:           0
    Total transferred:      18620000 bytes
    HTML transferred:       17780000 bytes
    Requests per second:    849.81 [#/sec] (mean)
    Time per request:       11.767 [ms] (mean)
    Time per request:       1.177 [ms] (mean, across all concurrent requests)
    Transfer rate:          1545.25 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.1      0       3
    Processing:     2   12   2.4     11      23
    Waiting:        2   11   2.3     11      23
    Total:          4   12   2.4     11      23
    

## NGINX RESULTS

For comparison, I ran against [nginx](http://www.nginx.org/) serving the same 
file. Note that the [rawk](/projects/rawk/) [site](http://www.brokenlcd.net) 
is served by nginx on the same VPS.

    $ ab -n 10000 -c 10 http://rawk.brokenlcd.net/

    Concurrency Level:      10
    Time taken for tests:   2.315 seconds
    Complete requests:      10000
    Failed requests:        0
    Write errors:           0
    Total transferred:      19900000 bytes
    HTML transferred:       17780000 bytes
    Requests per second:    4320.22 [#/sec] (mean)
    Time per request:       2.315 [ms] (mean)
    Time per request:       0.231 [ms] (mean, across all concurrent requests)
    Transfer rate:          8395.75 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        1    1   0.1      1       2
    Processing:     0    1   0.1      1       2
    Waiting:        0    1   0.3      1       2
    Total:          2    2   0.2      2       3

## NOTES

[nginx](http://www.nginx.org/) is about five times faster. This indicates
potential for a great amount of improvement.
