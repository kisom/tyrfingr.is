## SRVWD(1)
### minimal web server
- - -

## NAME
**srvwd** - serve static files in the working directory

## SYNOPSIS
**srvwd** is a minimal web server written in ANSI C that serves the current 
working directory. It can be chrooted for security, and can also be given a 
user and group to run as. It was designed for testing sites generated with 
rawk, and is not intended to be run as a production web server.


## INSTALLATION
To install **srvwd** globally:

    ./config.sh
    make && sudo make install

To install **srvwd** for the current user:

    PREFIX="${HOME}" ./config.sh 
    make && make install


## DOCUMENTATION
See the man page in the distribution (srvwd.1). The man page is also available
[online](/projects/srvwd/srvwd.1.html).


## SOURCE
* [distribution tarball](/distfiles/srvwd/srvwd-1.4.6.tgz) 
([signature](/distfiles/srvwd/srvwd-1.4.6.tgz.sig))
* source: <tt>git clone https://bitbucket.org/kisom/srvwd.git</tt>
* [bitbucket page](https://bitbucket.org/kisom/srvwd)


## SEE ALSO
[Minimal web development with rawk(1) and srvwd(1)](/essays/essay_minimal_webdev.html).


## LICENSE
**srvwd** is released under the [ISC license](/licenses/LICENSE.ISC).


## PERFORMANCE
There are performance metrics [available](/projects/srvwd/perf.html) comparing 
**srvwd** with [nginx](http://www.nginx.org/).


## TODO
0. add support for writing to a file instead of standard out.
0. add errors for POST
0. target request_handler for performance improvements
0. add support for escape characters


## CAVEATS
The core functionality of **srvwd** was written in three hours. It is lacking
in many features that many users would find useful.


## CONTRIBUTORS
[Lawrence Teo](http://www.lteo.net) and Jeremy helped with code reviews.
[Bryce Chidester](https://twitter.com/brycied00d) helped with testing for
performance improvements.

