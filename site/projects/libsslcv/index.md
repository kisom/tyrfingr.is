## LIBSSLCV(3)
### validate OpenSSL client certificates
- - -

## NAME
**libsslcv** - validate OpenSSL client certificates


## SYNOPSIS
**libsslcv** provides functions for validating client certificates
and loading PEM-encoded certficiates from a file. it is primarily
intended to serve as a reference implementation.


## INSTALLATION
To install **libsslcv** globally:

        sudo mk install
        or
        sudo make install

To install **libsslcv** for the current user:

        mk install PREFIX=${HOME}
        or
        PREFIX=${HOME} make install


## DOCUMENTATION
See the man page in the distribution (libsslcv.3). The man page is also
available [online](/projects/libsslcv/libsslcv.3.html).


## SOURCE
* [distribution tarball](/distfiles/libsslcv/libsslcv-1.0.2.tgz)
([signature](/distfiles/libsslcv/libsslcv-1.0.2.tgz.sig))
* source: <tt>hg clone http://hg.tyrfingr.is/libsslcv
* [bitbucket page](https://bitbucket.org/kisom/libsslcv)


## LICENSE
**libsslcv** is released under a 
[dual ISC/public domain license](/licenses/LICENSE.DUAL)
