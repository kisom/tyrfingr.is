## LIBDAEMON(3)
### libdaemon is a lightweight daemonisation library in C


## SYNOPSIS
**libdaemon** is a minimal and somewhat portable daemonisation library that 
works under OpenBSD and Linux. 


## DOCUMENTATION
See the [html documentation](/projects/libdaemon/libdaemon.html/). The
[man page](/projects/libdaemon/libdaemon.3.html) is available in html format 
as well.

**libdaemon** ships with texinfo and man page documentation.


## SOURCE
* [distribution tarball](/distfiles/libdaemon/libdaemon-3.1.0.tar.gz)
([signature](/distfiles/libdaemon/libdaemon-3.1.0.tar.gz.sig))
* source: <tt>git clone git@github.com:kisom/libdaemon.git</tt>
* [github page](https://github.com/kisom/libdaemon)


## LICENSE
**libdaemon** is released under an 
[ISC and public domain dual-license](/licenses/LICENSE.DUAL).
