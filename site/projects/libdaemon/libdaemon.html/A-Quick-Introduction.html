<html lang="en">
<head>
<title>A Quick Introduction - libdaemon 3.0.4</title>
<meta http-equiv="Content-Type" content="text/html">
<meta name="description" content="libdaemon 3.0.4">
<meta name="generator" content="makeinfo 4.8">
<link title="Top" rel="start" href="index.html#Top">
<link rel="prev" href="index.html#Top" title="Top">
<link rel="next" href="Things-You-Should-Know.html#Things-You-Should-Know" title="Things You Should Know">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<!--
This manual is for libdaemon, a small and somewhat portable C daemonisation
library that runs under OpenBSD and Linux.

Copyright (C) 2011 Kyle Isom <<kyle@tyrfingr.is>>

     This document is released under a dual license. These two licenses
     are the public domain and a Creative Commons
     Attribution-ShareAlike 3.0 Unported License. The purpose of this
     dual-license is to attribute you, the end user, to use this
     documentation for whatever purpose you wish. As such, you, the end
     user, are free to select either license at will.

     This document is released into the public domain.

     This work is licensed under the Creative Commons
     Attribution-ShareAlike 3.0 Unported License. To view a copy of
     this license, visit http://creativecommons.org/licenses/by-sa/3.0/
     or send a letter to Creative Commons, 444 Castro Street, Suite
     900, Mountain View, California, 94041, USA.
   -->
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<div class="node">
<p>
<a name="A-Quick-Introduction"></a>
Next:&nbsp;<a rel="next" accesskey="n" href="Things-You-Should-Know.html#Things-You-Should-Know">Things You Should Know</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="index.html#Top">Top</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="index.html#Top">Top</a>
<hr>
</div>

<h2 class="chapter">1 A Quick Introduction</h2>

<p><a name="index-intro-1"></a><a name="index-introduction-to-_0040command_007blibdaemon_007d-2"></a>
Libdaemon is a minimal and somewhat portable daemonisation library that
works under OpenBSD and Linux. It may also run under FreeBSD and NetBSD,
but this has not been tested.

   <p>Libdaemon is distributed with the <code>daemon.h</code> header which must be included
to make use of the library, as well as linking to <code>libdaemon.a</code>.

<h3 class="section">1.1 Set up and daemonising</h3>

<p>The first call to Libdaemon should be a call to <code>init_daemon</code>. This has
the prototype
<pre class="example">     int                      init_daemon(char *, uid_t, gid_t);
</pre>
   <p>The first <code>char *</code> should be the rundir variable (see &ldquo;The run
directory&rdquo;) which may be <code>NULL</code> to have Libdaemon generate a sane
default. The second and third arguments are the user and group IDs to run as,
respectively. If set to 0, the value will be the same as the user running the
daemon.

   <p><code>init_daemon</code> returns <code>EXIT_SUCCESS</code> if all went well, and the
configuration struct (<code>struct libdaemon_config</code> in <code>daemon.h</code>) may
be accessed using <code>daemon_getconfig(void)</code>. Of note is that a
<code>struct libdaemon_config</code> struct has the member <code>char *rundir</code>, which
may come in handy later. See &ldquo;The libdaemon_config struct&rdquo;.

<h3 class="section">1.2 The libdaemon_config struct</h3>

<p>The definition for <code>struct libdaemon_config</code> is this:
<pre class="example">     struct libdaemon_config {
             char    *rundir;
             char    *pidfile;
             char    *logfile;
             int      logfd;
             uid_t   run_uid;
             gid_t   run_gid;
     
     };
</pre>
   <p><code>char *rundir</code> is the run directory the daemon is using (see The run
directory). This can be used for future calls; for example, the libdaemon core
uses <code>static struct libdaemon_config *cfg</code>, and passes <code>cfg-&gt;rundir</code>
as the argument to several functions during the initialisation and run phases.

   <p><code>char *pidfile</code> is the file containing the daemon's PID. The library will
create the file <code>cfg-&gt;rundir/__progname.pid</code>; this value is stored in the
configuration struct for future use (although a call to <code>getpid</code> could
also be used).

   <p><code>char *logfile</code> is the file the daemon logs (as opposed to using the
<code>syslog</code> interface). Correspondingly, <code>int logfd</code> is the file
descriptor for the logfile. If syslog is being used, <code>logfile</code> will be
NULL and <code>logfd</code> will be -1. Otherwise, they will point to a file on disk
to be used.

   <p><code>uid_t run_uid</code> and <code>gid_t run_gid</code> contain the UID and GID the
daemon was run as, and are used internally for sanity checks. You may use these
or, just as easily, <code>getuid()</code> or <code>getgid()</code>.

<h3 class="section">1.3 Logging</h3>

<p>Libdaemon provides an internal logging framework:

<pre class="example">     int                      daemon_setlog(char *logfile);
     int                      daemon_log(int, char *);
     int                      daemon_vlog(int, char *, ...);
</pre>
   <p><code>daemon_setlog</code> is used to open a log file. If one is currently open, it
will be closed first. If the <code>char *</code> is NULL, the daemon will use the
syslog interface. The function returns <code>EXIT_SUCCESS</code> if the logfile was
sucessfully opened and any previous open logfiles were successfully closed.

   <p><code>daemon_log</code> and <code>daemon_vlog</code> are used to log information to
<code>logfile</code>. The <code>int</code> is the log level to use, which is one of
<code>DAEMON_INFO</code>, <code>DAEMON_WARN</code>, <code>DAEMON_ERR</code>, <code>DAEMON_ALERT</code>,
or <code>DAEMON_EMERG</code>. These are mapped to their <code>LOG_*</code> equivalents, and
are provided so your code does not need to include <code>syslog.h</code>. 
<code>daemon_log</code> is used to write simple <code>char *</code> strings to the log,
and <code>daemon_vlog</code> is used to write more complex (i.e. formatted) log
messages. The first <code>char *</code> in <code>daemon_vlog</code> should be the format,
and any following arguments should be those required by the format. 
<code>daemon_vlog</code> keeps count of the number of arguments passed in and the
number of conversion specifications (ex. <code>%d</code>) to make sure the number
of arguments matches the format. Both functions return <code>EXIT_SUCCESS</code> if
the log was successfully written to, and <code>EXIT_FAILURE</code> if there was an
error.

   </body></html>

