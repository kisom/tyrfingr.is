## RMTMP(1)
### remove temporary files


## NAME
**rmtmp** - remove temporary files


## SYNOPSIS
**rmtmp** is a small utility written in C designed to clean up temporary
files. There is a less performant Go version available, as well.


## INSTALLATION
To install **srm** globally:

    mk && mk install

To install **srm** for the current user:

    mk && mk BIN=${HOME}/bin MAN=${HOME}/share/man install


## DOCUMENTATION
See the man page in the distribution (rmtmp.1). The man page is also
available [online](/projects/rmtmp/rmtmp.1.html).


## SOURCE
* [distribution tarball](/distfiles/rmtmp/rmtmp-1.0.3.tgz)
([signature](/distfiles/rmtmp/rmtmp-1.0.3.tgz.sig))
* source: <tt>hg clone https://bitbucket.org/kisom/rmtmp</tt>
* [bitbucket page](https://bitbucket.org/kisom/rmtmp)
* [Go version's bitbucket page](https://bitbucket.org/kisom/rmtmp.go)


## HISTORY
**rmtmp** was written to clean up after a prototype program failed to
clean up its temporary files, leading to over 2M temporary files in
`/tmp/`; the standard Unix tools failed to properly handle this in
time. The first version of **rmtmp** was written in just under fifteen
minutes, and took five minutes to clean up the temporary directory. While
singular in its purpose and aimed at a very specific use case, it is
uploaded here in the case it is useful for others.


## LICENSE
**rmtmp** is released under the [ISC license](/licenses/LICENSE.ISC).
