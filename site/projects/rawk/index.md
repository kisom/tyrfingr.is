## rawk(1) 
### static site generator
- - -

## NAME
**rawk** - rage against web frameworks

## SYNOPSIS
**rawk** is a static site generator written in the bourne shell. It uses POSIX 
compliant syntax to support the maximum number of systems and requires only a 
markdown parser and the standard UNIX tools (grep and sed in particular).


## INSTALL
To install **rawk** globally:
    ./config.sh
    sudo make 

To install **rawk** for the current user:
    PREFIX=${HOME} ./config.sh
    make
    
To use rawk, ensure a markdown parser is installed / available


## DOCUMENTATION
Man pages:
* [rawk(1)](/projects/rawk/rawk.1.html)
* [rawkrc(5)](/projects/rawk/rawkrc.5.html)

See the README in the distribution file. **rawk** also has a 
[site](http://rawk.brokenlcd.net).


## SEE ALSO
[Minimal web development with rawk(1) and srvwd(1)](/essays/essay_minimal_webdev.html).

[Why rawk?](http://rawk.brokenlcd.net/why.html)

## SOURCE
* [distribution tarball](/distfiles/rawk/rawk-1.2.9.tgz)
([signature](/distfiles/rawk/rawk-1.2.9.tgz.sig))
* source: <tt>git clone git@github.com:kisom/rawk.git</tt>
* [github page](https://github.com/kisom/rawk)


## LICENSE
**rawk** is released under a [dual ISC/public domain license](/licenses/LICENSE.DUAL).

