## BUILD_RELEASE(1)
### build custom OpenBSD install CDs
- - -

## NAME
**build_release** - build custom OpenBSD releases

## SYNOPSIS
**build_release** is designed to build a custom install image equivalent to
the installXX.iso image offered by the OpenBSD team.


## INSTALLATION
To install **build_release** globally:

    ./config.sh
    sudo make install

To install **build_release** for the current user:

    PREFIX="${HOME}" ./config.sh 
    make install


## DOCUMENTATION
See the man page in the distribution (build\_release.1). The man page is also 
available [online](/projects/build_release/build_release.1.html).


## SOURCE
* [distribution tarball](/distfiles/build_release/build_release-1.2.0.tgz)
([signature](/distfiles/build_release/build_release-1.2.0.tgz.sig))
* source: <tt>git clone https://bitbucket.org/kisom/build_release.git</tt>
* [bitbucket page](https://bitbucket.org/kisom/build_release)


## LICENSE
**build_release** is released under the [ISC license](/licenses/LICENSE.ISC).


## TODO
0. Build release does not yet support building snapshots.
