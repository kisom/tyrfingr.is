## SWP(1)
### utility to swap a pair of files
- - -

## NAME
**swp** - swap two files

## SYNOPSIS
**swp** takes two file names, and swaps their contents. It is written
in the [go](http://www.golang.org) programming language.


## INSTALLATION
To install **swp** globally:

    mk install

To install **swp** for the current user:

    mk install PREFIX=${HOME}


## DOCUMENTATION
See the man page in the distribution (swp.1). The man page is also available
[online](/projects/swp/swp.1.html).


## SOURCE
* [distribution tarball](/distfiles/swp/swp-1.0.0.tgz)
([signature](/distfiles/swp/swp-1.0.0.tgz.sig))
* source (hg): <tt>hg clone https://bitbucket.org/kisom/swp</tt>
* source (go): <tt>go get bitbucket.org/kisom/swp</tt>
* [bitbucket page](https://bitbucket.org/kisom/swp)


## LICENSE
**swp** is released under a [dual public domain ISC license](/licenses/LICENSE.DUAL).

