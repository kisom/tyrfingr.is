## CSKEL(7)
### skeleton files for new C projects
- - -

## NAME
**cskel** - C project skeleton

## SYNOPSIS
**cskel** is a skeleton set of files that are useful when making a new C 
project. It includes a config.sh and Makefile.in that are used to generate code
that will build properly on Linux (which requires some hand holding as opposed
to the BSDs). It also has a man page template and a copy of the ISC license.


## USAGE

    mkdir new_project
    cp /path/to/cskel/* new_project


## SOURCE
* [distribution tarball](/distfiles/cskel/cskel-1.1.4.tgz)
([signature](/distfiles/cskel-cskel-1.1.4.tgz.sig))
* source: <tt>git clone https://bitbucket.org/kisom/cskel.git</tt>
* [bitbucket page](https://bitbucket.org/kisom/cskel)


## LICENSE
**cskel** is released under the [ISC license](/licenses/LICENSE.ISC).


