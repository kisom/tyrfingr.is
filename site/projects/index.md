## PROJECTS 

[build_release](/projects/build_release/) - custom OpenBSD install iso creator.

[cskel](/projects/cskel/) - skeleton files for new C projects.

[dhkam](/projects/dhkam/) - various implementations of Diffie-Hellman key
exchange.

[libbase64](/projects/libbase64/) - statically compiled base64 library written
in C.

[libdaemon](/projects/libdaemon/) - a lightweight daemon framework written in C.

[libsslcv](/projects/libsslcv/) - a reference library for validating
OpenSSL client certificates written in C.

[rawk](/projects/rawk/) - static site generator written in the bourne shell.

[rmtmp](/projects/rmtmp/) - remove temporary files.

[srm](/projects/srm/) - a simple secure file deletion utility written in C.

[srvwd](/projects/srvwd/) - minimal web server written in C.

[swp](/projects/swp/) - simple utility to swap two files written in Go and
using plan9's mk.


## HGWEB
Many of the projects are available via the [hgweb](http://hg.tyrfingr.is)
interface.


## CODE SIGNING

All of the distribution files are signed by my 
[development GnuPG key](/keys/coder.asc) with fingerprint:

    pub   4096R/B7B720D6 2010-10-30 [expires: 2015-10-29]
          Key fingerprint = 9C2B BF53 FB89 50D4 554F  1708 35C8 B8B3 B7B7 20D6
          uid                  Kyle Isom <coder@kyleisom.net>
          sub   4096R/F73968C2 2010-10-30 [expires: 2015-10-29]


## LICENSE

All the code on this site is released under either an 
[ISC license](/licenses/LICENSE.ISC) or under an
[ISC / public domain dual license](/licenses/LICENSE.DUAL).
