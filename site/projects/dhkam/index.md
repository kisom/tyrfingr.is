## DHKAM(3)
### blinded Diffie-Hellman key exchange method

## NAME
**dhkam(3)** -- blinded Diffie-Hellman key exchange method

## DESCRIPTION

This is a pair of implementations of blinded DHKAM on the RFC 3526 group 14
modulus: a Go and a C version. The C version is a statically compiled library
(36K unstripped via gcc version 4.7.2).


## INSTALLATION

### Go

    go get hg.tyrfingr.is/gokyle/dhkam
    go get bitbucket.org/gokyle/dhkam
    go get github.com/gokyle/dhkam

### C

    ./configure && make && make install

## DOCUMENTATION

* Go version via [godoc.org](http://godoc.org/github.com/gokyle/dhkam).
* The C version ships with a man page and a texinfo manual.

## SOURCE
### Go Version

* [Tyrfingr](http://hg.tyrfingr.is/dhkam)
* [Bitbucket](https://bitbucket.org/kisom/dhkam)
* [Github](https://github.com/gokyle/dhkam)

### C Version

* [distribution tarball](/distfiles/libdhkam/libdhkam-1.0.0.tar.gz) ([signature](/distfiles/libdhkam/libdhkam-1.0.0.tar.gz.sig))
* [Tyrfingr](http://hg.tyrfingr.is/libdhkam)
* [Bitbucket](https://bitbucket.org/kisom/libdhkam)
* [Github](https://github.com/kisom/libdhkam)


## LICENSE

dhkam is released under an ISC license.


## ECDH

ECDH is elliptic curve Diffie-Hellman example implementation in Go.

### INSTALLATION

    go get hg.tyrfingr.is/gokyle/ecdh
    go get bitbucket.org/gokyle/ecdh
    go get github.com/gokyle/ecdh

