## SRM(1)
### secure file deletion utility
- - -

## NAME
**srm** - securely delete files

## SYNOPSIS
**srm** is a simple secure file deletion utility. It overwrites the file with
several passes of random data before unlinking it.


## INSTALLATION
To install **srm** globally:

    ./config.sh
    make && sudo make install

To install **srm** for the current user:

    PREFIX="${HOME}" ./config.sh 
    make && make install


## DOCUMENTATION
See the man page in the distribution (srm.1). The man page is also available
[online](/projects/srm/srm.1.html).


## SOURCE
* [distribution tarball](/distfiles/srm/srm-1.3.0.tgz)
([signature](/distfiles/srm/srm-1.3.0.tgz.sig))
* source: <tt>git clone https://bitbucket.org/kisom/srm.git</tt>
* [bitbucket page](https://bitbucket.org/kisom/srm)


## HISTORY
**srm** was written when I needed a secure file removal tool, but didn't
have a network connection to download one. Ergo, **srm** was born.


## LICENSE
**srm** is released under the [ISC license](/licenses/LICENSE.ISC).
