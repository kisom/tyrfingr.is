## PORTFOLIO

### CODE

I've collected here a few examples I think are representative of both my
coding style and my familiarity with Unix. The difficulty with my current
employer is that all of the work I do is closed source and under NDA, so
most of these are aimed at scratching an itch or demonstrating particular
skills. These have all been done outside of work. The links here point to
the project pages on this site, which contain brief descriptions of the
project, links to the relevant source control pages (i.e. on Bitucket or
Github), and distribution tarballs.

* [libbase64](/projects/libbase64/) - static C99 base64 library.
* [rmtmp](/projects/rmtmp/) - C utility to remove temporary files.
* [srm](/projects/srm/) - a simple secure file deletion utility written in
  C.
* [srvwd](/projects/srvwd/) - minimal web server written in ANSI C.
* [dhkam](/projects/dhkam) - a pair of implementations of Diffie-Hellman
key exchange in Go and C. There is also an example implementation of
elliptic curve Diffie-Hellman in Go.

My work is heavily influenced by the OpenBSD project and my work on embedded
Linux systems; I typically aim for high quality code; as fewer lines of code
translate to fewer bugs, I strive to write the minimal amount of code to
effectively accomplish the program's purpose. I also believe in the Unix
philosophy of "do one thing and do it well."

All of my projects come with a license, a README, and, in the case of
installable C code (and often with other languages as well), man pages.
My libraries ship with tests to validate the code being provided as well.
All of the projects ship with a Makefile; some also include a Plan 9
mkfile as well. The projects vary in their usage of the autotools; many
of the projects using autotools also ship with Texinfo documentation.

There are a few projects under development or on hold that will eventually make
their way here. I am mentioning them because they are relevant to some of the
skills presented here:

* What is now [Practical Cryptography with Go](http://gokyle.org/book/) started
  as an ANSI C / libgcrypt-based project that laid much of the foundation for
  later work. The decision to move to Go was done at an early stage; however,
  the current state of the book is [available on Github](https://github.com/kisom/c_crypto_intro).
* I have done some work with Linux kernel modules; however, much of my
  knowledge is on the early 2.6 series of kernels. I am working to bring
  this up to date with a Linux implementation of Plan 9's `/dev/time` device.

These projects are being done in my spare time while I am also trying to write
a book; there is little guarantee that they will be done in a timely manner.

### ARTICLES

I've written a few articles of note, primarily published via my main
[blog](http://kyleisom.net):

* [The Zen of C](/essays/essay_zen_of_c.html)
* [Coders (Should) Do Much More Than Code](http://kyleisom.net/blog/2011/09/10/coders-should-do-much-more-than-code/)
* [Network Autoconfiguration with Go and ØMQ](http://kyleisom.net/blog/2013/02/26/network-autoconfiguration-with-go-and-zmq/)
