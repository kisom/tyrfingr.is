<pre style="text-align: center;">
 ____ ____ ____ ____ ____               
||n |||o |||t |||e |||s ||              
||__|||__|||__|||__|||__||              
|/__\|/__\|/__\|/__\|/__\|              

</pre>

"[writing static libraries in C](/notes/notes_staticlib.html)" - more focused
on writing the Makefile, using [ar(1)](http://www.openbsd.org/cgi-bin/man.cgi?query=ar&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html) 
and considerations for the code. (2012.05.21)

"[generating patchfiles with git and mercurial](/notes/notes_patchfiles.html)"
 - how to generate patch files using git and mercurial.    
(2011.09.28)

