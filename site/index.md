
## SYNOPSIS
tyrfingr is a famous sword in Icelandic mythology. UNIX is my weapon of choice 
when hacking, and this site serves as a portfolio of various UNIX projects.
Most of the code is in C, but you will find some other languages (mostly
shell and perl) mixed in.


## PROJECTS
See the [projects](/projects/) page; several repositories are available
via the [hgweb](http://hg.tyrfingr.is) interface.

The [crypto](/crypto) page has some of the crypto projects I've
worked on.

## WRITING
I've written some [essays](/essays.html) and some technical [notes](/notes.html)
for various topics (more along the lines of HOWTOs). 
They are very likely to still be in an alpha state.


## CONTRIBUTIONS
I've [contributed](/contrib.html) to a few open source projects.


## SEE ALSO
[OpenBSD](http://www.openbsd.org) - my preferred operating system.


## AUTHOR
[Kyle Isom](/about.html) &lt;kyle at tyrfingr dot is&gt;.

[Personal GnuPG key](/keys/tyrfingr.asc) fingerprint:

    pub   4096R/1A192B06 2012-04-23 [expires: 2017-04-22]
          Key fingerprint = 7F0E BC23 B266 1C80 ECAF  0A8D D6CC 8A90 1A19 2B06
          uid                  Kyle Isom (personal key) <kyle@tyrfingr.is>
          sub   4096R/E5A6633F 2012-04-23 [expires: 2017-04-22]

I have a [personal site](http://www.kyleisom.net/),
[bitbucket account](https://bitbucket.org/kisom/), and a
[github account](https://github.com/kisom/).




