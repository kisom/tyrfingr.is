## NAME
**Kyle Isom** - software security engineer 

## RESUME
My resume is available in [mandoc](/files/resume/kyleisom.7), 
[plaintext](/files/resume/kyleisom.txt), 
[HTML](/files/resume/kyleisom.html), and [PDF](/files/resume/kyleisom.pdf)
format. A 
[tarball](/files/resume/kyleisom_resume-2012-06-09.tar.gz)
([sig](/files/resume/kyleisom_resume-2012-06-09.tar.gz.sig)) 
with all four formats is available as is the 
[source](/files/resume/kyleisom_resume_src-2012-06-09.tar.gz)
([signature](/files/resume/kyleisom_resume_src-2012-06-09.tar.gz.sig)).

## DESCRIPTION
I started off with [NetBSD](http://www.netbsd.org) via [SDF](http://www.sdf.org)
around 2000, which was the first UNIX I was exposed to. Later on, I was able
to install various Linux distributions via the CDs in the backs of various 
books from the local library. I ended up going back to running BSD when I got 
my second Thinkpad (a T20) before leaving for university in 2005. Around 2006, 
I started running [OpenBSD](http://www.openbsd.org/).

I prefer writing plain C and using the standard UNIX tools (sh, awk, sed, and
so forth) to get things done.

## HARDWARE
I recently upgraded from a Lenovo Thinkpad T410 to a Lenovo Thinkpad
X230, both running OpenBSD; but sometimes I failover to my late 2011
13" Macbook Air. While I like the Air because it tends to just work,
I'm much happier on OpenBSD.  I upgraded my Thinkpad to use a 64G
solid state drive and I will never go back. I've got 8GB of RAM in the
thing. I read things on my iPad, which is mostly technical books via
O'Reilly, No Starch Press, Manning Press, and anyone else offering
DRM-free ebooks. I listen to music on my iPod classic on a set of
Beyerdynamic DT-770s to cut out the world.

## SOFTWARE 
I use [spectrwm](http://opensource.conformal.com/wiki/spectrwm/) by
[conformal](http://www.conformal.com) as my window manager, which is
started by *xdm(1)*. My shell is [zsh](http://www.zsh.org), which I
almost always use in conjunction with the
[tmux](http://tmux.sourceforge.net) terminal multiplexer.  It's not
uncommon to have five or more tmux sessions with numerous windows open
(it was often joked in university that I only ran X to have four
terminals open side by side). I use [vim](http://www.vim.org/) as my
primary editor, primarily via the graphical version because I love
having tabs. However, I also use
[emacs](http://www.gnu.org/software/emacs/) when writing lisp code
(lately, mostly Clojure) because of
[SLIME](http://common-lisp.net/project/slime/). For web browsing, I
use [lynx](http://lynx.isc.org/),
[links](http://www.jikos.cz/~mikulas/links/), and
[xxxterm](http://opensource.conformal.com/wiki/spectrwm/) for web
browsing.  For reading PDFs, I've become a fan of
[apvlv](http://naihe2010.github.com/apvlv/). I listen to music via
[cmus](http://cmus.sourceforge.net/); for most network messaging
protocols, I use a combination of [irssi](http://www.irssi.org/),
[bitlbee](http://www.bitlbee.org/main.php/news.r.html), and the
[irssi silc plugin](http://silcnet.org/) to communicate over
[IRC](http://www.ietf.org/rfc/rfc1459.txt),
[silc](http://silcnet.org/), [xmpp](http://www.xmpp.org/), and OSCAR /
AIM (though that is being slowly deprecated). I run irssi on a tmux
session on a [prgmr](http://www.prgmr.com/) VPS.


